# The Komputer Store

### Possibilities

- Work and earn money
- Bank your money
- Get a loan
- Repay your loan
- Find your new favorite laptop – and buy it!

#

### How to run app

1. Clone repo
2. Run with live-server or equivalent

#

### Live demo

https://simonsutterud.gitlab.io/the-komputer-store/
