///////////////////
// ELEMENTS

// Buttons
const getLoanBtn = document.querySelector(".get-loan-btn");
const depositBankBtn = document.querySelector(".deposit-bank-btn");
const workBtn = document.querySelector(".work-btn");
const buyLaptopBtn = document.querySelector(".buy-laptop-btn");
const repayLoanBtn = document.querySelector(".repay-loan-btn");

// Text
const bankBalanceEl = document.querySelector(".bank-balance");
const payBalanceEl = document.querySelector(".pay-balance");
const loanBalanceEl = document.querySelector(".loan-balance");
const outstandingLoanEls = document.querySelector(".outstanding-loan");
const laptopTitleEl = document.querySelector(".laptop-title");
const laptopDescEl = document.querySelector(".laptop-desc");
const laptopPriceEl = document.querySelector(".laptop-price");

// Misc.
const mainEl = document.querySelector("main");
const laptopSelectEl = document.querySelector("#laptops");
const featuresListEl = document.querySelector(".laptop-features-list");
const laptopImgEl = document.querySelector(".laptop-img");

///////////////////
// GLOBAL VARIABLES

// Array for storing laptops
const laptopsArr = [];

// Variable for storing currently selected laptop
let currentLaptop;

///////////////////
// Class for storing user info
class User {
	constructor() {
		this.bankBalance = 0;
		this.payBalance = 0;
		this.loanBalance = 0;
	}

	hasLoan() {
		return this.loanBalance > 0;
	}

	work() {
		this.payBalance += 100;
		return this.payBalance;
	}

	repayLoan() {
		this.loanBalance -= this.payBalance;
		this.payBalance = 0;
		this.#checkLoanNotNegative();
		return this.loanBalance;
	}

	// Checking whether too much of the loan was paid back
	// If too much was paid, the excess amount is returned to user's bank
	#checkLoanNotNegative() {
		if (this.loanBalance < 0) {
			this.bankBalance -= this.loanBalance;
			this.loanBalance = 0;
		}
	}

	depositToBank() {
		if (this.hasLoan()) {
			const deductAmt = Math.round(this.payBalance * 0.1);
			this.payBalance -= deductAmt;
			this.loanBalance -= deductAmt;
			this.#checkLoanNotNegative();
		}
		this.bankBalance += this.payBalance;
		this.payBalance = 0;
	}

	isEligibleForLoan(amt) {
		return +amt / 2 > this.bankBalance || !this.hasLoan();
	}

	requestLoan(amt) {
		this.bankBalance += +amt;
		this.loanBalance += +amt;
	}

	buyLaptop(price) {
		if (price <= this.bankBalance) {
			this.bankBalance -= price;
			return { ok: true };
		}
		return { ok: false };
	}
}

///////////////////
// FUNCTIONS

const fetchLaptops = async () => {
	try {
		const res = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers");
		if (!res.ok) throw new Error("Fetch failed!");
		const laptops = await res.json();
		return laptops;
	} catch (error) {
		alert("An error occurred while fetching the laptops: " + error);
	}
};

const addBaseUrlToImageUrls = (laptops) => {
	laptops.forEach(
		(laptop) => (laptop.image = "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image)
	);
	return laptops;
};

const renderBalance = (element, balance) => (element.innerText = balance + " kr");

const renderLaptopSelectOptions = (laptops) => {
	laptops.forEach((laptop) => {
		const optionEl = document.createElement("option");
		optionEl.value = laptop.title;
		optionEl.innerText = laptop.title;
		optionEl.dataset.id = laptop.id;
		laptopSelectEl.appendChild(optionEl);
	});
};

const renderLaptopFeatures = (laptop) => {
	const { specs: features } = laptop;
	featuresListEl.innerHTML = "";
	features.forEach((feature) => {
		const li = document.createElement("li");
		li.innerText = "- " + feature;
		featuresListEl.appendChild(li);
	});
};

const renderLaptopImage = (laptop) => {
	const { image, title } = laptop;
	laptopImgEl.src = image;
	laptopImgEl.alt = title;
};

const renderLaptopTitle = (laptop) => {
	const { title } = laptop;
	laptopTitleEl.innerText = title;
};

const renderLaptopDesc = (laptop) => {
	const { description: desc } = laptop;
	laptopDescEl.innerText = desc;
};

const renderLaptopPrice = (laptop) => {
	const { price } = laptop;
	laptopPriceEl.innerText = price + " NOK";
};

const getSelectedLaptop = (event) => {
	const laptopId = +event.target.selectedOptions[0].dataset.id;
	const selectedLaptop = laptopsArr.find((laptop) => laptop.id === laptopId);
	return selectedLaptop;
};

const renderAllLaptopInfo = (laptop) => {
	renderLaptopFeatures(laptop);
	renderLaptopImage(laptop);
	renderLaptopTitle(laptop);
	renderLaptopDesc(laptop);
	renderLaptopPrice(laptop);
};

const toggleLoanCtrls = () => {
	if (user.hasLoan()) {
		repayLoanBtn.style.opacity = 1;
		outstandingLoanEls.style.opacity = 1;
		outstandingLoanEls.style.pointerEvents = "all";
		repayLoanBtn.style.pointerEvents = "all";
	} else {
		repayLoanBtn.style.opacity = 0;
		outstandingLoanEls.style.opacity = 0;
		outstandingLoanEls.style.pointerEvents = "none";
		repayLoanBtn.style.pointerEvents = "none";
	}
};

const pageFadeIn = () => {
	mainEl.style.opacity = 1;
	mainEl.style.pointerEvents = "all";
};

///////////////////
// EVENT LISTENERS

workBtn.addEventListener("click", () => {
	user.work();
	renderBalance(payBalanceEl, user.payBalance);
});

getLoanBtn.addEventListener("click", () => {
	const loanAmt = prompt("Enter loan amount:");
	if (!user.isEligibleForLoan(loanAmt))
		return alert("Sorry! You are not eligible to receive a loan :(");

	user.requestLoan(loanAmt);
	renderBalance(payBalanceEl, user.payBalance);
	renderBalance(bankBalanceEl, user.bankBalance);
	renderBalance(loanBalanceEl, user.loanBalance);
	toggleLoanCtrls();
});

repayLoanBtn.addEventListener("click", () => {
	user.repayLoan();
	renderBalance(payBalanceEl, user.payBalance);
	renderBalance(bankBalanceEl, user.bankBalance);
	renderBalance(loanBalanceEl, user.loanBalance);
	toggleLoanCtrls();
});

depositBankBtn.addEventListener("click", () => {
	user.depositToBank();
	renderBalance(payBalanceEl, user.payBalance);
	renderBalance(bankBalanceEl, user.bankBalance);
	renderBalance(loanBalanceEl, user.loanBalance);
});

buyLaptopBtn.addEventListener("click", function (event) {
	const { title, price } = currentLaptop;
	if (user.buyLaptop(price).ok) {
		renderBalance(bankBalanceEl, user.bankBalance);
		alert(`Congrats!! You are now the owner of a ${title}!`);
	} else alert("Sorry! You cannot afford this laptop.");
});

laptopSelectEl.addEventListener("change", function (event) {
	currentLaptop = getSelectedLaptop(event);
	renderAllLaptopInfo(currentLaptop);
});

///////////////////
// INITIALIZE APPLICATION

const user = new User();

(async () => {
	let laptops = await fetchLaptops();
	laptops = addBaseUrlToImageUrls(laptops);
	laptopsArr.push(...laptops);
	renderLaptopSelectOptions(laptops);
	currentLaptop = laptops[0];
	renderAllLaptopInfo(laptops[0]);
	pageFadeIn();
})();
